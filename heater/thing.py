import time

import machine
import network
import ubinascii
from machine import Pin

import pinmap


def client_id():
    return ubinascii.hexlify(machine.unique_id()).decode('ascii')


def init_wifi(config):
    for retry in init_wifi_(config):
        time.sleep(1.0)


def init_wifi_(config):
    # disable access point
    network.WLAN(network.AP_IF).active(False)

    wifi = network.WLAN(network.STA_IF)

    if not wifi.isconnected():
        wifi.active(True)
        wifi.connect(config.NAME, config.PASSWORD)
        while not wifi.isconnected():
            yield


def init_wifi_flash_led(config):
    status_led = Pin(pinmap.LED, Pin.OUT)
    flash_led(status_led, 0.9)
    for retry in init_wifi_(config):
        flash_led(status_led, 0.1)
    flash_led(status_led, 0.5)
    flash_led(status_led, 0.5)


def flash_led(led, duty_on, cycle=1.0):
    led.off()  # inverted?!
    time.sleep(duty_on)
    led.on()
    time.sleep(cycle - duty_on)
