import sys
import pytest
from unittest.mock import Mock, call, patch


@pytest.fixture
def comms():
    for module in ['time', 'umqtt_simple', 'machine', 'network', 'ubinascii']:
        sys.modules[module] = Mock()

    sys.modules['umqtt_simple'].MQTTClient = Mock()

    if 'comms' in sys.modules:
        del sys.modules['comms']

    import comms

    return comms


def test_ticks(comms):
    ticks = comms.Ticks()

    calls = []
    def func():
        calls.append(comms.time.time())

    ticks.register(5, func)

    for i in range(30):
        comms.time.time.return_value = 1000 + i
        ticks.tick()

    assert calls == [1000, 1005, 1010, 1015, 1020, 1025]


def test_ticks2(comms):
    ticks = comms.Ticks()

    calls1 = []
    def func1():
        calls1.append(comms.time.time())

    calls2 = []
    def func2():
        calls2.append(comms.time.time())

    ticks.register(5, func1)
    ticks.register(7, func2)

    for i in range(30):
        comms.time.time.return_value = 1000 + i
        ticks.tick()

    assert calls1 == [1000, 1005, 1010, 1015, 1020, 1025]
    assert calls2 == [1000, 1007, 1014, 1021, 1028]


def test_messages(comms):
    client = comms.Client('123', 'server')

    callback = Mock()

    client.subscribe(['topic'], callback)

    client._callback(b'topic', '"message"')
    client._callback(b'topic', '"message"')
    client._callback(b'topic', '123.4')

    assert callback.mock_calls == [call('"message"', topics=['topic']),
                                   call('123.4', topics=['topic'])]


def test_messages_promise(comms):
    client = comms.Client('123', 'server')

    callback = Mock()

    client.subscribe(['one', 'two'], callback)

    client._callback(b'one', '"a"')
    client._callback(b'one', '"b"')
    client._callback(b'two', '"c"')
    client._callback(b'two', '"d"')

    assert callback.mock_calls == [call('"b"', '"c"', topics=['one', 'two']),
                                   call('"b"', '"d"', topics=['one', 'two'])]
