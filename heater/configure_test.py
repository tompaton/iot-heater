from unittest.mock import Mock, call

import configure


def _node(node, inputs, keys, output, interval, params):
    return "NodeHandler({}({}), inputs=[{}], keys={}, output={}, interval={})".format(
        node,
        ", ".join(
            "{}={}".format(key, _str(value)) for key, value in sorted(params.items())
        ),
        ", ".join(_str(input) for input in inputs),
        keys,
        _str(output),
        interval,
    )


def _str(param):
    return repr(param)


def test__configure():
    config = {
        "places/taylor-st": {
            "living-room": {
                "temperature": "Temperature(interval=60)",
                "!red-led": "LED(../../../status/c3011c00, pin=15, on='due')",
            },
            "heater": {
                # "mode": "Input('OFF')",
                # "state": "Input('home')",
                # "states": "Dict(wake=21,away=0,home=21,sleep=16)",
                # "manual-target": "Input(18)",
                "auto-target": "Lookup(states, state)",
                "target": "Case(mode, 'MANUAL', manual-target, 'ON', auto-target, 0)",
                "relay": "Compare(../living-room/temperature, target, 'ON', 'OFF', 'OFF')",
                "!relay": "Relay(relay)",
                "!display": "Display('State:', state, 'Temp:', ../living-room/temperature)",
            },
        }
    }

    assert sorted([_node(*args) for args in configure._configure(config, "")]) == [
        "NodeHandler(Case(), inputs=['places/taylor-st/heater/mode', 'MANUAL', 'places/taylor-st/heater/manual-target', 'ON', 'places/taylor-st/heater/auto-target', 0.0], keys=[0, 2, 4], output='places/taylor-st/heater/target', interval=None)",
        "NodeHandler(Compare(), inputs=['places/taylor-st/living-room/temperature', 'places/taylor-st/heater/target', 'ON', 'OFF', 'OFF'], keys=[0, 1], output='places/taylor-st/heater/relay', interval=None)",
        "NodeHandler(Display(), inputs=['State:', 'places/taylor-st/heater/state', 'Temp:', 'places/taylor-st/living-room/temperature'], keys=[1, 3], output=None, interval=None)",
        "NodeHandler(LED(on='due', pin=15.0), inputs=['status/c3011c00'], keys=[0], output=None, interval=None)",
        "NodeHandler(Lookup(), inputs=['places/taylor-st/heater/states', 'places/taylor-st/heater/state'], keys=[0, 1], output='places/taylor-st/heater/auto-target', interval=None)",
        "NodeHandler(Relay(), inputs=['places/taylor-st/heater/relay'], keys=[0], output=None, interval=None)",
        "NodeHandler(Temperature(), inputs=[], keys=[], output='places/taylor-st/living-room/temperature', interval=60.0)",
    ]
