import sys
import time
from unittest.mock import Mock, call, patch

import pytest


@pytest.fixture
def nodes():
    for module in [
        "dht",
        "sht30",
        "ssd1306",
        "machine",
        "micropython",
        "hp303b",
        "network",
    ]:
        sys.modules[module] = Mock()

    if "nodes" in sys.modules:
        del sys.modules["nodes"]

    import nodes

    return nodes


def test_node_init(nodes):
    client = Mock()
    handler = nodes.NodeHandler(
        client, ["input1", "input2"], [0], "output", nodes.Lookup()
    )

    assert client.mock_calls == [
        call.log("Lookup input1 -> output"),
        call.subscribe(["input1"], handler._handle),
    ]


def test_node__handle(nodes):
    client = Mock()
    node = Mock()
    node.handle.return_value = "result"
    handler = nodes.NodeHandler(client, ["input1", "input2", 12.3], [0], "output", node)

    handler._handle('"abc"')

    node.handle.assert_called_with("abc", "input2", 12.3)

    client.publish.assert_called_with(handler.output, msg='"result"')


def test_node__handle2(nodes):
    client = Mock()
    node = Mock()
    node.handle.return_value = "result"
    handler = nodes.NodeHandler(client, ["input1"], [0], "output", node)

    handler._handle(b"ONLINE 1.0.0")

    node.handle.assert_called_with("ONLINE 1.0.0")

    client.publish.assert_called_with(handler.output, msg='"result"')


@pytest.mark.parametrize(
    "value,result",
    [
        ("5", '"True"'),
        ("10", '"Equal"'),
        ("15", '"False"'),
    ],
)
def test_compare(nodes, value, result):
    client = Mock()
    node = nodes.NodeHandler(
        client, ["input1", 10, "True", "Equal", "False"], [0], "output", nodes.Compare()
    )

    node._handle(value)

    client.publish.assert_called_with(node.output, msg=result)


@pytest.mark.parametrize(
    "value,result",
    [
        ('"A"', "1.0"),
        ('"B"', "2.0"),
        ('"C"', "3.0"),
    ],
)
def test_case(nodes, value, result):
    client = Mock()
    node = nodes.NodeHandler(
        client, ["input1", "A", 1.0, "B", 2.0, 3.0], [0], "output", nodes.Case()
    )

    node._handle(value)

    client.publish.assert_called_with(node.output, msg=result)


@pytest.mark.parametrize(
    "value,result",
    [
        ('"A"', "1"),
        ('"B"', "2"),
        ('"C"', "3"),
    ],
)
def test_lookup(nodes, value, result):
    client = Mock()
    node = nodes.NodeHandler(client, ["dict", "key"], [0, 1], "output", nodes.Lookup())

    node._handle('{"A": 1, "B": 2, "C": 3}', value)

    client.publish.assert_called_with(node.output, msg=result)


def test_temperature_sht(nodes):
    client = Mock()

    node = nodes.TemperatureSHT()
    node.sensor.measure.return_value = 12.3456, 34.5678

    handler = nodes.NodeHandler(client, [], [], "output", node, 10)

    handler._handle()

    assert node.sensor.mock_calls == [call.measure()]

    assert client.mock_calls[0] == call.log("TemperatureSHT  -> output")
    assert client.mock_calls[1] == call.tick(10, handler._handle)
    assert client.mock_calls[2] == call.publish(
        handler.output, msg='{"temperature": 12.3, "humidity": 34.6}'
    ) or client.mock_calls[2] == call.publish(
        handler.output, msg='{"humidity": 34.6, "temperature": 12.3}'
    )


def test_barometer(nodes):
    client = Mock()

    node = nodes.Barometer()
    node.sensor.read.return_value = 12.3456, 103456.8

    handler = nodes.NodeHandler(client, [], [], "output", node, 10)

    handler._handle()

    assert node.sensor.mock_calls == [call.setup(), call.read()]

    assert client.mock_calls[0] == call.log("Barometer  -> output")
    assert client.mock_calls[1] == call.tick(10, handler._handle)
    assert client.mock_calls[2] == call.publish(
        handler.output, msg='{"temperature": 12.3, "pressure": 1034.6}'
    ) or client.mock_calls[2] == call.publish(
        handler.output, msg='{"pressure": 1034.6, "temperature": 12.3}'
    )


def test_wifi_rssi(nodes):
    client = Mock()

    node = nodes.WifiRSSI()
    node.sta_if.status.return_value = -67

    handler = nodes.NodeHandler(client, [], [], "output", node, 10)

    handler._handle()

    assert nodes.network.mock_calls == [
        call.WLAN(nodes.network.STA_IF),
        call.WLAN().status("rssi"),
    ]

    assert client.mock_calls[0] == call.log("WifiRSSI  -> output")
    assert client.mock_calls[1] == call.tick(10, handler._handle)
    assert client.mock_calls[2] == call.publish(handler.output, msg='{"rssi": -67}')


@pytest.mark.parametrize(
    "value,relay,result",
    [
        ('"ON"', call.on(), "relay ON"),
        ('"OFF"', call.off(), "relay OFF"),
    ],
)
def test_relay(nodes, value, relay, result):
    client = Mock()
    node = nodes.Relay()
    handler = nodes.NodeHandler(client, ["input1"], [0], None, node)

    handler._handle(value)

    assert node.relay.mock_calls == [relay]
    assert client.mock_calls == [
        call.log("Relay input1 -> None"),
        call.subscribe(["input1"], handler._handle),
        # call.log(result)
    ]


@pytest.mark.parametrize(
    "value,led,result",
    [
        ('"ON"', call.on(), "led ON"),
        ('"OFF"', call.off(), "led OFF"),
    ],
)
def test_led(nodes, value, led, result):
    client = Mock()
    node = nodes.LED(on="ON", pin=15.0)
    handler = nodes.NodeHandler(client, ["input1"], [0], None, node)

    handler._handle(value)

    assert node.led.mock_calls == [led]
    assert client.mock_calls == [
        call.log("LED input1 -> None"),
        call.subscribe(["input1"], handler._handle),
        # call.log(result)
    ]


def test_display(nodes):
    client = Mock()
    node = nodes.Display()
    handler = nodes.NodeHandler(
        client, ["Label:", "input1", "input2"], [1, 2], None, node
    )

    handler._handle('"value"', "12.3")

    assert node.display.mock_calls == [
        call.fill(0),
        call.text("Label:", 0, 0),
        call.text("value", 0, 12),
        call.text("12.3", 0, 24),
        call.show(),
    ]

    assert client.mock_calls == [
        call.log("Display input1 input2 -> None"),
        call.subscribe(["input1", "input2"], handler._handle),
    ]


def test_average(nodes):
    client = Mock()
    node = nodes.NodeHandler(client, ["input1"], [0], "output", nodes.Average(window=2))

    for value in range(4):
        node._handle(str(value))

    assert client.mock_calls == [
        call.log("Average input1 -> output"),
        call.subscribe(["input1"], node._handle),
        call.publish(node.output, msg="0.5"),
        call.publish(node.output, msg="2.5"),
    ]


def test_duty(nodes):
    client = Mock()
    node = nodes.NodeHandler(client, ["input1"], [0], "output", nodes.Duty())

    with patch("time.time") as faketime:
        for ts, value in [
            (10, '"OFF"'),
            (20, '"OFF"'),
            (30, '"ON"'),
            (35, '"ON"'),
            (40, '"OFF"'),
        ]:
            faketime.return_value = ts
            node._handle(value)

    assert client.mock_calls[0] == call.log("Duty input1 -> output")
    assert client.mock_calls[1] == call.subscribe(["input1"], node._handle)

    assert client.mock_calls[2] in (
        call.publish(node.output, msg='{"value": "OFF", "duration": 20}'),
        call.publish(node.output, msg='{"duration": 20, "value": "OFF"}'),
    )

    assert client.mock_calls[3] in (
        call.publish(node.output, msg='{"value": "ON", "duration": 10}'),
        call.publish(node.output, msg='{"duration": 10, "value": "ON"}'),
    )

    assert len(client.mock_calls) == 4


@pytest.mark.parametrize(
    "value,duty",
    [
        ("40", 40.0),
        ("60.0", 60.0),
    ],
)
def test_servo(nodes, value, duty):
    client = Mock()
    node = nodes.Servo(pin=3)
    handler = nodes.NodeHandler(client, ["input1"], [0], None, node)

    handler._handle(value)

    assert nodes.PWM.mock_calls == [call(nodes.Pin(3), freq=50), call().duty(duty)]

    assert client.mock_calls == [
        call.log("Servo input1 -> None"),
        call.subscribe(["input1"], handler._handle),
    ]


@pytest.mark.parametrize(
    "kwargs,reading,result",
    [
        ({}, 512, "50.0"),
        ({}, 0, "0.0"),
        ({}, 1024, "100.0"),
        ({"min": 250, "max": 750}, 350, "20.0"),
        ({"min": 750, "max": 250}, 350, "80.0"),
    ],
)
def test_adc(nodes, kwargs, reading, result):
    client = Mock()

    node = nodes.AnalogInput(**kwargs)
    node.sensor.read.return_value = reading

    handler = nodes.NodeHandler(client, [], [], "output", node, 10)

    handler._handle()

    assert node.sensor.mock_calls == [call.read()]

    assert client.mock_calls[0] == call.log("AnalogInput  -> output")
    assert client.mock_calls[1] == call.tick(10, handler._handle)
    assert client.mock_calls[2] == call.publish(handler.output, msg=result)


def test_debounce_init(nodes):
    client = Mock()

    node = nodes.DebouncedSwitch()

    handler = nodes.NodeHandler(client, [], [], "output", node)

    assert client.mock_calls == [call.log("DebouncedSwitch  -> output")]

    assert nodes.Pin.mock_calls[0] == call(0, nodes.Pin.IN, nodes.Pin.PULL_UP)
    assert node.pin.mock_calls == [call.irq(handler=node.irq_cb)]
    assert node.timer.mock_calls == [call.init(period=100, callback=node.tick_cb)]


def test_debounce_handle(nodes):
    client = Mock()

    node = nodes.DebouncedSwitch()

    handler = nodes.NodeHandler(client, [], [], "output", node)

    node._handle(None)

    assert client.mock_calls == [
        call.log("DebouncedSwitch  -> output"),
        call.publish("output", msg="1.0"),
    ]


@pytest.mark.parametrize(
    "cbs,count",
    [
        (".1...", 1),
        (".111...", 1),  # debounced
        (".11.1...", 1),  # debounced
        (".11..2...", 2),  # two signals
        ("1..2..3..4/..5..", 5),  # overflow
        (".11----1...", 1),  # switch held down
        (".11--.-1...", 1),  # dropped interrupts
        (".11-..-2...", 2),  # dropped interrupts
    ],
)
def test_debounce_interrupt(nodes, cbs, count):
    client = Mock()

    node = nodes.DebouncedSwitch()

    handler = nodes.NodeHandler(client, [], [], "output", node)

    for cb in cbs:
        if cb in ".-":
            node.pin.value.return_value = int(cb == ".")
            node.tick_cb(node.timer)
        elif cb == "/":
            # test overflow
            node.count = 0
        else:
            node.irq_cb(node.pin)

    assert nodes.micropython.mock_calls == [call.schedule(node._handle, None)] * count
