import gc

import mqtt_config
import wifi_config

import comms
import configure
import hp303b
import nodes
import sht30
import ssd1306
import thing

gc.collect()

client = comms.Client(
    thing.client_id(),
    mqtt_config.SERVER,
    user=mqtt_config.UID,
    password=mqtt_config.PWD,
    wifi=wifi_config,
)

client.loop(version="1.5.0")
