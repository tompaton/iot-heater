import json


def child(parent, key):
    path = (tuple(parent.split("/"))
            if parent else ()) + tuple(key.split("/"))
    while '..' in path:
        index = path.index('..')
        path = path[:index - 1] + path[index + 1:]
    topic = '/'.join(path)
    if '!' in topic:
        topic = '!' + topic.replace('!', '')
    return topic


def to_string(value):
    return value.strip("'").strip('"')


def is_string(value):
    return value.startswith("'") or value.startswith('"')


def is_numeric(value):
    return value.lstrip('-').replace('.', '', 1).isdigit()


def configure(client, nodes, config_raw):
    # convert to list to flatten call stack
    config = list(_configure(json.loads(config_raw), ''))

    result = []
    for node, inputs, keys, output, interval, params in config:
        if hasattr(nodes, node):
            result.append(
                nodes.NodeHandler(client, inputs, keys, output,
                                  getattr(nodes, node)(**params), interval))
        else:
            client.log('UNKNOWN: {} {} -> {}'
                       .format(node,
                               ' '.join([inputs[key] for key in keys]),
                               output))
    return result


def _configure(config, parent_topic):
    for key, value in config.items():
        key2 = child(parent_topic, key)

        if isinstance(value, dict):
            yield from _configure(value, key2)

        else:
            node, params_ = value.rstrip(')').split('(', 1)

            inputs = []
            keys = []

            if not key2.startswith('!'):
                output = key2
            else:
                output = None

            interval = None

            params = {}
            if params_:
                for param in params_.split(','):
                    param = param.strip()
                    if is_string(param):
                        inputs.append(to_string(param))
                    elif '=' in param:
                        param2, value2 = param.split('=', 1)
                        param2 = param2.strip()
                        value2 = value2.strip()
                        if is_string(value2):
                            params[param2] = to_string(value2)
                        elif param2 == 'interval':
                            interval = float(value2)
                        else:
                            params[param2] = float(value2)
                    elif is_numeric(param):
                        inputs.append(float(param))
                    else:
                        keys.append(len(inputs))
                        inputs.append(child(parent_topic, param))

            yield node, inputs, keys, output, interval, params
