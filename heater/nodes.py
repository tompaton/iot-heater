import json
import time

import micropython
import network
from machine import ADC, I2C, PWM, Pin, Timer

import hp303b
import sht30
import ssd1306


class NodeHandler:
    def __init__(self, client, inputs, keys, output, node, interval=None):
        self.client = client
        self.inputs = inputs
        self.output = output
        self.node = node

        self.keys = keys

        self.client.log(
            "{} {} -> {}".format(
                self.node.__class__.__name__,
                " ".join([self.inputs[key] for key in self.keys]),
                self.output,
            )
        )

        if self.keys:
            self.client.subscribe([self.inputs[key] for key in self.keys], self._handle)

        if interval:
            self.client.tick(interval, self._handle)

        if hasattr(self.node, "bind"):
            self.node.bind(self)

    def _handle(self, *args, topics=None):
        for i, arg in enumerate(args):
            try:
                self.inputs[self.keys[i]] = json.loads(arg)
            except ValueError:
                self.inputs[self.keys[i]] = arg.decode("ascii")
        msg = self.node.handle(*self.inputs)
        if msg is not None and self.output is not None:
            self.client.publish(self.output, msg=json.dumps(msg))

    def reset(self):
        if hasattr(self.node, "reset"):
            self.node.reset()


class TemperatureSHT:
    def __init__(self):
        # TODO: config args for driver
        self.sensor = sht30.SHT30()

    def handle(self):
        temp, humid = self.sensor.measure()
        return {"temperature": round(temp, 1), "humidity": round(humid, 1)}


class Barometer:
    def __init__(self):
        bus = I2C(scl=Pin(5), sda=Pin(4), freq=100000)
        bus.start()

        self.sensor = hp303b.HP303B(bus)
        self.sensor.setup()

    def handle(self):
        temp, pressure = self.sensor.read()
        return {"temperature": round(temp, 1), "pressure": round(pressure / 100, 1)}


class AnalogInput:
    def __init__(self, min=0, max=1024):
        self.sensor = ADC(0)
        self.min = min
        self.scale = 100 / (max - min)

    def handle(self):
        val = self.sensor.read()
        return round((val - self.min) * self.scale, 1)


class WifiRSSI:
    def __init__(self):
        self.sta_if = network.WLAN(network.STA_IF)

    def handle(self):
        return {"rssi": self.sta_if.status("rssi")}


class Relay:
    def __init__(self, pin=4, on="ON", off="OFF"):
        self.on = on
        self.off = off

        self.relay = Pin(pin, Pin.OUT)

    # TODO: pass client into handle()?
    def handle(self, relay):
        if relay == self.on:
            self.relay.on()
            # self.client.log('relay ON')

        if relay == self.off:
            self.relay.off()
            # self.client.log('relay OFF')

    def reset(self):
        self.relay.off()


class LED:
    def __init__(self, pin=0, on="ON"):
        self.on = on
        self.led = Pin(int(pin), Pin.OUT)

    def handle(self, led):
        if led == self.on:
            self.led.on()
        else:
            self.led.off()

    def reset(self):
        self.led.off()


class Display:
    def __init__(self, scl_pin=5, sca_pin=4):
        # TODO: config args for driver, dimensions etc.
        i2c = I2C(-1, Pin(scl_pin), Pin(sca_pin))
        self.display = ssd1306.SSD1306_I2C(64, 48, i2c)
        self.line_height = 12

    def handle(self, *text):
        self.display.fill(0)
        for y, line in enumerate(text):
            self.display.text(str(line), 0, y * self.line_height)
        self.display.show()


class Lookup:
    @staticmethod
    def handle(values, key):
        return values[key]


class Case:
    @staticmethod
    def handle(var, *args):
        for i in range(0, len(args) - 1, 2):
            if args[i] == var:
                return args[i + 1]
        return args[-1]


class Compare:
    @staticmethod
    def handle(value_1, value_2, result_less, result_equal, result_more):
        if float(value_1) < float(value_2):
            return result_less
        if float(value_1) == float(value_2):
            return result_equal
        if float(value_1) > float(value_2):
            return result_more


class Average:
    def __init__(self, window=5):
        self.window = window
        self.total = 0.0
        self.count = 0

    def handle(self, value):
        self.count += 1
        self.total += value

        if self.count == self.window:
            average = self.total / self.count
            self.total = 0.0
            self.count = 0
            return average


class Duty:
    def __init__(self):
        self.value = None
        self.start = None

    def handle(self, value):
        result = None

        if self.value != value:
            now = time.time()

            if self.start is not None:
                result = {"value": self.value, "duration": now - self.start}

            self.value = value
            self.start = now

        return result


class Servo:
    def __init__(self, pin=4):
        self.servo = PWM(Pin(int(pin)), freq=50)

    def handle(self, duty):
        self.servo.duty(duty)


class DebouncedSwitch:
    def __init__(self, value=1.0, pin=0, period=100):
        self.value = value
        self.pin = Pin(int(pin), Pin.IN, Pin.PULL_UP)
        self.timer = Timer(-1)
        self.period = period

        self.count = self.ready1 = self.ready2 = 0

        self._handle = self.handle
        self.client = None
        self.output = None

    def bind(self, handler):
        self.client = handler.client
        self.output = handler.output

        self.pin.irq(handler=self.irq_cb)
        self.timer.init(period=self.period, callback=self.tick_cb)

    def reset(self):
        self.pin.irq(handler=None)
        self.timer.deinit()

    def irq_cb(self, pin):
        self.count += 1

    def tick_cb(self, t):
        # ignore until switch is off
        if not self.pin.value():
            return

        if self.ready1 != self.count:
            # got a trigger, need to wait until it's stable
            self.ready1 = self.count

            # self.count (and self.ready1) could have overflowed and be less
            # than self.ready2 now.

            # assert self.ready1 != self.ready2

        elif self.ready2 != self.ready1:
            # has been stable for 2 ticks

            # if we were interrupted between if and elif, then self.ready1 !=
            # self.count any more, but that's ok as we'll count another pulse
            # next time around.

            # assert self.ready1 <= self.count (unless self.count has overflowed
            # during interrupt)

            self.ready2 = self.ready1

            micropython.schedule(self._handle, None)

        else:
            # assert self.ready2 == self.ready1 <= self.count (unless self.count
            # has overflowed during interrupt)
            pass

    def handle(self, arg):
        self.client.publish(self.output, msg=json.dumps(self.value))
