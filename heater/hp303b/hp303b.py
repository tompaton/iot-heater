"""
Basic MicroPython module to read temperature and pressure from
HP303b sensor on D1 mini Barometric Pressure Shield.

https://docs.wemos.cc/en/latest/d1_mini_shiled/barometric_pressure.html

Data sheet:
https://docs.wemos.cc/en/latest/_static/files/hp303b_datasheet.pdf
"""

class HP303B:

    # from machine import I2C, Pin
    # i2c_bus = I2C(scl=Pin(5), sda=Pin(4), freq=100000)
    # i2c_bus.start()

    def __init__(self, i2c_bus, addr=0x77):
        self.bus = i2c_bus
        self.addr = addr
        self.kT = None
        self.kP = None
        self.calculate_temperature = None
        self.calculate_pressure = None

    def read_reg(self, reg, mask):
        # print(hex(reg))
        # print(mask)
        out = 0
        sign = 0
        buf = [b for b in self.bus.readfrom_mem(self.addr, reg, len(mask))]
        # print(buf)
        for b, m in zip(buf, mask):
            if not m & 0x0F:
                # print('high4')
                out = out << 4
                out = out + ((b & m) >> 4)
                sign += 4
            elif not m & 0xF0:
                # print('low4')
                out = out << 4
                out = out + (b & m)
                sign += 4
            else:
                # print('8bit')
                out = out << 8
                out = out + (b & m)
                sign += 8
        # print(sign)
        # print(out)
        if out & (1 << (sign - 1)):
            # print('neg')
            out -= 1 << sign
        # print(out)
        return out

    def write_reg(self, reg, value):
        self.bus.writeto_mem(self.addr, reg, value)

    def wait_reg(self, mask):
        while True:
            buf = [b for b in self.bus.readfrom_mem(self.addr, 0x08, 1)]
            # print('MEAS_CFG={}'.format(buf[0]))
            if buf[0] & mask:
                return

    def setup(self):
        product_id = self.bus.readfrom_mem(self.addr, 0x0D, 1)
        if product_id != b'\x10':
            raise ValueError('Unknown product_id={}'.format(product_id))

        self.init_coefficients()
        self.configure()

    def init_coefficients(self):
        # Coefficients Addr. bit7 bit6 bit5 bit4 bit3 bit2 bit1 bit0
        # c0      0x10 c0  [11:4]
        # c0/c1   0x11 c0  [3:0] c1 [11:8]
        # c1      0x12 c1  [7:0]
        # c00     0x13 c00 [19:12]
        # c00     0x14 c00 [11:4]
        # c00/c10 0x15 c00 [3:0] c10 [19:16]
        # c10     0x16 c10 [15:8]
        # c10     0x17 c10 [7:0]
        # c01     0x18 c01 [15:8]
        # c01     0x19 c01 [7:0]
        # c11     0x1A c11 [15:8]
        # c11     0x1B c11 [7:0]
        # c20     0x1C c20 [15:8]
        # c20     0x1D c20 [7:0]
        # c21     0x1E c21 [15:8]
        # c21     0x1F c21 [7:0]
        # c30     0x20 c30 [15:8]
        # c30     0x21 c30 [7:0]

        self.wait_reg(0x80)  # COEF_RDY
        c0 = self.read_reg(0x10, b'\xFF\xF0')
        c1 = self.read_reg(0x11, b'\x0F\xFF')
        c00 = self.read_reg(0x13, b'\xFF\xFF\xF0')
        c10 = self.read_reg(0x15, b'\x0F\xFF\xFF')
        c01 = self.read_reg(0x18, b'\xFF\xFF')
        c11 = self.read_reg(0x1A, b'\xFF\xFF')
        c20 = self.read_reg(0x1C, b'\xFF\xFF')
        c21 = self.read_reg(0x1E, b'\xFF\xFF')
        c30 = self.read_reg(0x20, b'\xFF\xFF')

        # print('c0={}'.format(c0))
        # print('c1={}'.format(c1))
        # print('c00={}'.format(c00))
        # print('c10={}'.format(c10))
        # print('c01={}'.format(c01))
        # print('c11={}'.format(c11))
        # print('c20={}'.format(c20))
        # print('c21={}'.format(c21))
        # print('c30={}'.format(c30))

        def calculate_temperature(Traw_sc):
            return c0 * 0.5 + c1 * Traw_sc

        def calculate_pressure(Traw_sc, Praw_sc):
            return (c00
                    + Praw_sc * (c10 + Praw_sc * (c20 + Praw_sc * c30))
                    + Traw_sc * c01
                    + Traw_sc * Praw_sc * (c11 + Praw_sc * c21))

        self.calculate_temperature = calculate_temperature
        self.calculate_pressure = calculate_pressure

    def configure(self):
        # TODO: support different measurement and oversampling rates

        # pressure configuration PRS_CFG
        # measurement rate PM_RATE: 000 - 1 measurement/sec
        # oversampling rate PM_PRC: 0001 - 2 times
        self.write_reg(0x06, b'\x01')
        self.kP = 1572864
        # temperature configuration TMP_CFG
        # TMP_EXT: 1 - external sensor (in pressure sensor)
        # measurement rate TMP_RATE: 000 - 1 measurement/sec
        # oversampling rate TM_PRC: 0000 - 1 time
        self.write_reg(0x07, b'\x80')
        self.kT = 524288
        # configuration CFG_REG
        # 0 - no interupts, fifo etc.
        self.write_reg(0x09, b'\x00')

        self.wait_reg(0x40)  # SENSOR_RDY

    def read(self):
        # read temperature TMP
        # MEAS_CTRL: 010 - temperature measurement
        self.write_reg(0x08, b'\x02')
        self.wait_reg(0x20)  # TMP_RDY
        Traw = self.read_reg(0x03, b'\xFF\xFF\xFF')
        # print('Traw={}'.format(Traw))
        # calculate temperature
        Traw_sc = Traw / self.kT
        # print('Traw_sc={}'.format(Traw_sc))
        Tcomp = self.calculate_temperature(Traw_sc)
        # print('Tcomp={}'.format(Tcomp))

        # read pressure PSR
        # MEAS_CTRL: 001 - pressure measurement
        self.write_reg(0x08, b'\x01')
        self.wait_reg(0x10)  # PRS_RDY
        Praw = self.read_reg(0x00, b'\xFF\xFF\xFF')
        # print('Praw={}'.format(Praw))
        # calculate pressure
        Praw_sc = Praw / self.kP
        # print('Praw_sc={}'.format(Praw_sc))
        Pcomp = self.calculate_pressure(Traw_sc, Praw_sc)
        # print('Pcomp={}'.format(Pcomp))

        return (Tcomp, Pcomp)
