import gc
import time

from umqtt_simple import MQTTClient

from machine import Pin, reset

import pinmap

import thing


class Ticks:
    def __init__(self):
        self._ticks = []
        self._last_run = {}

    def reset(self):
        self._ticks.clear()
        self._last_run.clear()

    def register(self, interval, function):
        self._ticks.append((interval, function))
        self._last_run[id(function)] = 0

    def tick(self):
        for interval, function in self._ticks:
            now = time.time()
            if now - self._last_run[id(function)] >= interval:
                self._last_run[id(function)] = now
                function()


class Client:
    def __init__(self, client_id, server, keepalive=60,
                 user=None, password=None, wifi=None):
        self.client_id = client_id
        self.keepalive = keepalive
        self._client = None
        self.server = server
        self.user = user
        self.password = password
        self.wifi_config = wifi

        self.ticks = Ticks()
        self.nodes = []

        self._callbacks = {}
        self._cache = {}

    def reset(self):
        #print("reset")
        self.ticks.reset()
        # TODO: properly reset config - this will still be subscribed to all the
        # messages, but we'll ignore them and/or the handler will be different
        # from last time.
        self._callbacks.clear()
        self._cache.clear()

        self.reset_nodes()

        # subscribe to config topic to bootstrap configuration
        self.subscribe(['config/' + self.client_id], self.handle_config,
                       immediate=True)

        # keep alive
        self.tick(self.keepalive // 2, self.ping)

    def reset_nodes(self):
        for node in self.nodes:
            node.reset()

    def subscribe(self, topics, callback, immediate=False):
        for topic in topics:
            if topic not in self._callbacks:
                #print('subscribe ' + topic)
                if immediate:
                    self._client.subscribe(topic)
                self._callbacks[topic] = []
            self._callbacks[topic].append((callback, topics))

    def add_subscriptions(self):
        for topic in self._callbacks:
            self._client.subscribe(topic)

    def _callback(self, topic, msg_raw):
        topic = topic.decode('ascii')

        #print('topic: ' + topic)
        # TODO: support wildcards
        if topic not in self._callbacks:
            # ignore messages we aren't expecting (from previous config)
            #print('unknown')
            return

        # if unchanged, ignore message
        if self._cache.get(topic) == msg_raw:
            #print('same')
            return

        self._cache[topic] = msg_raw
        for callback, args in self._callbacks[topic]:
            # do callback if all arguments available
            vals = [self._cache.get(arg) for arg in args]
            if all(vals):
                callback(*vals, topics=args)

    def handle_config(self, msg_raw, topics=None):
        self.log('{} {}'.format(topics[0], gc.mem_free()))

        # TODO: handle changes to config
        #self.reset()

        # don't want add_subscriptions to subscribe to /config again or it will
        # resend the retained message
        self._callbacks.clear()

        import configure, nodes
        self.nodes = configure.configure(self, nodes, msg_raw)

        self.add_subscriptions()

        self.log('free {}'.format(gc.mem_free()))

    def loop(self, version):
        _error = None

        status_led = Pin(pinmap.LED, Pin.OUT)
        status_led.off()  # ON!

        while True:
            try:
                self.connect()
                status_led.on()  # OFF!

                if _error != None:
                    self.error(str(_error))
                    _error = None

                #print('online...')
                self.status("ONLINE " + version)

                self.reset()

                gc.collect()

                while True:
                    self._client.check_msg()
                    self.ticks.tick()
                    time.sleep(1)

            except Exception as err:
                _error = str(err)
                #print('loop error:')
                #print(_error)

                status_led.off()  # ON!

                try:
                    # reset relay in case it takes a while to get back online
                    self.reset_nodes()

                    self.status("OFFLINE")
                    self._client.disconnect()

                except:
                    pass

                self.delay()

    def connect(self):
        self._client = None
        while self._client == None:
            #print('wifi...')
            thing.init_wifi(self.wifi_config)
            #print('wifi connected')
            time.sleep(1)

            #print('mqtt...')
            try:
                self._client = MQTTClient(self.client_id,
                                          self.server,
                                          keepalive=self.keepalive,
                                          user=self.user,
                                          password=self.password)
                self._client.set_callback(self._callback)
                self._client.set_last_will(**self._status("OFFLINE"))

                #print('mqtt connecting...')
                self._client.connect()

            except Exception as err:
                self._client = None
                #print('mqtt error:')
                #print(str(err))
                self.delay()

        #print('mqtt connected')

    def delay(self):
        #print('sleeping...')
        # random to prevent DDOS attack on mqtt server!
        time.sleep(10)

    def tick(self, interval, function):
        self.ticks.register(interval, function)

    def log(self, msg):
        self.publish("log/" + self.client_id, msg=msg)

    def error(self, msg):
        self.publish("error/" + self.client_id, msg=msg)
        if 'memory allocation failed' in msg:
            self.log('reset')
            reset()

    def ping(self):
        self._client.ping()

    def publish(self, topic, msg, **kwargs):
        # TODO: write into _cache? or trigger _callback?
        #print('publish ' + topic + ' ' + msg)
        self._client.publish(topic, msg, **kwargs)

    def status(self, msg):
        self.publish(**self._status(msg))

    def _status(self, msg):
        return {'topic': 'status/' + self.client_id,
                'msg': msg, 'retain': True, 'qos': 1}
